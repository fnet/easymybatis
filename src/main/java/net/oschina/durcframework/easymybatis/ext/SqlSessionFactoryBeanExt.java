/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext;

import static org.springframework.util.Assert.notNull;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.core.io.Resource;

import net.oschina.durcframework.easymybatis.query.QueryConfig;


/**
 * SqlSessionFactoryBean扩展
 * @author tanghc
 *
 */
public class SqlSessionFactoryBeanExt extends SqlSessionFactoryBean {
	
	private MapperLocationsBuilder mapperLocationsBuilder = new MapperLocationsBuilder();
	
	private String basePackage;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
		
		mapperLocationsBuilder.setDataSource(dataSource);
	}
	
	@Override
	public void setMapperLocations(Resource[] mapperLocations) {
		mapperLocationsBuilder.storeMapperFile(mapperLocations);
	}
	
	@Override
	protected SqlSessionFactory buildSqlSessionFactory() throws IOException {
		notNull(this.basePackage, "属性 'basePackage' 必填");
		
		Resource[] allMapperLocations = mapperLocationsBuilder.build(this.basePackage);
		// 重新设置mapperLocation属性
		super.setMapperLocations(allMapperLocations);
		
		return super.buildSqlSessionFactory();
	}
	
	
	/**
	 * @param basePackage
	 *            指定哪些包需要被扫描,支持多个包"package.a,package.b"并对每个包都会递归搜索
	 */
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	/**
	 * 设置commonSqlClasspath文件位置，可选项。默认位置：easymybatis/commonSql.xml
	 * @param commonSqlClasspath
	 */
	public void setCommonSqlClasspath(String commonSqlClasspath) {
		this.mapperLocationsBuilder.setCommonSqlClasspath(commonSqlClasspath);
	}
	
	/**
	 * 设置模板文件位置，可选项
	 * @param templateClasspath
	 */
	public void setTemplateClasspath(String templateClasspath) {
		this.mapperLocationsBuilder.setTemplateClasspath(templateClasspath);
	}

	/**
	 * 设置默认的pageSize，可选项，默认10
	 * @param defaultPageSize
	 */
	public void setDefaultPageSize(int defaultPageSize) {
		QueryConfig.setDefaultPageSize(defaultPageSize);
	}

}
