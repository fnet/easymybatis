/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext.code.generator;

import java.util.List;

import net.oschina.durcframework.easymybatis.ext.code.util.FieldUtil;

/**
 * SQL上下文,这里可以取到表,字段信息<br>
 * 最终会把SQL上下文信息放到velocity中
 */
public class SQLContext {
	private TableDefinition tableDefinition; // 表结构定义
	private String namespace;
	private String packageName; // 包名
	private String classSimpleName;
	private String className;// java类完整路径,即:class.getName();

	public SQLContext(TableDefinition tableDefinition) {
		this.tableDefinition = tableDefinition;
	}

	public String getClassSimpleName() {
		return classSimpleName;
	}

	public void setClassSimpleName(String classSimpleName) {
		this.classSimpleName = classSimpleName;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * 返回Java类名
	 * 
	 * @return
	 */
	public String getJavaBeanName() {
		return classSimpleName;
	}

	/**
	 * 返回Java类名且首字母小写
	 * 
	 * @return
	 */
	public String getJavaBeanNameLF() {
		return FieldUtil.lowerFirstLetter(this.getJavaBeanName());
	}

	public String getPkName() {
		if (this.tableDefinition.getPkColumn() != null) {
			return this.tableDefinition.getPkColumn().getColumnName();
		}
		return "";
	}

	public String getJavaPkName() {
		if (this.tableDefinition.getPkColumn() != null) {
			return this.tableDefinition.getPkColumn().getJavaFieldName();
		}
		return "";
	}

	public String getJavaPkType() {
		if (this.tableDefinition.getPkColumn() != null) {
			return this.tableDefinition.getPkColumn().getJavaType();
		}
		return "";
	}

	public String getJavaPkTypeBox() {
		if (this.tableDefinition.getPkColumn() != null) {
			return this.tableDefinition.getPkColumn().getJavaTypeBox();
		}
		return "";
	}

	public String getMybatisPkType() {
		if (this.tableDefinition.getPkColumn() != null) {
			return this.tableDefinition.getPkColumn().getMybatisJdbcType();
		}
		return "";
	}

	public TableDefinition getTableDefinition() {
		return tableDefinition;
	}

	public void setTableDefinition(TableDefinition tableDefinition) {
		this.tableDefinition = tableDefinition;
	}

	public List<ColumnDefinition> getColumnDefinitionList() {
		return tableDefinition.getColumnDefinitions();
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public static void main(String[] args) {

		SQLContext[] tt = { new SQLContext(new TableDefinition("r_table_b")),
				new SQLContext(new TableDefinition("MyTable")), new SQLContext(new TableDefinition("user.frontUser")),
				new SQLContext(new TableDefinition("user.back_user")) };

		for (SQLContext ctx : tt) {
			System.out.println(ctx.getJavaBeanName());
		}
		/*
		 * 输出: RTableB MyTable UserFrontUser UserBackUser
		 */
	}

}
