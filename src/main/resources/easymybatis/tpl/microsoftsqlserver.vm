#set($jq="$")
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${context.namespace}">
    <resultMap id="queryResultMap" type="${context.className}">
        #foreach($column in $allColumns)
        <result column="${column.columnName}" property="${column.javaFieldName}" jdbcType="${column.mybatisJdbcType}" />
        #end
    </resultMap>
    
    <!-- 表字段 -->
    <sql id="baseColumns">
    	#set ($i=0) 
        #foreach($column in $columns)
        	#if($i > 0),#end t.[${column.columnName}]
			#set($i=$i +1)             
        #end
    </sql>
    
    <!-- 聚合查询 -->
    <select id="findProjection" parameterType="net.oschina.durcframework.easymybatis.query.projection.ProjectionQuery" resultType="java.util.HashMap">
		SELECT
			<include refid="common.projectionColumn"/>
		FROM ${table.tableName} t
		<include refid="common.where"/>  
		<include refid="common.groupBy"/>
		<include refid="common.having"/>
		<include refid="common.orderBy" />
	</select>

	<!-- 根据条件查询记录 -->
	<select id="find" parameterType="net.oschina.durcframework.easymybatis.query.Pageable"
		resultMap="queryResultMap">
		<if test="!isQueryAll">
			SELECT t0.* FROM (
		</if>
		SELECT row_number() over(
			<choose>
				<when test="orderable">ORDER BY ${jq}{order}</when>
				<otherwise>ORDER BY [${context.pkName}]</otherwise>
			</choose>
		) row,
                
       <include refid="common.sqlColumns" />
        
		FROM ${table.tableName} t
		<include refid="common.where" />
 		<if test="!isQueryAll">
			) t0 WHERE t0.row BETWEEN (#{start,jdbcType=INTEGER} +1) AND
			(#{start,jdbcType=INTEGER}  + #{limit,jdbcType=INTEGER})
		</if>
	</select>

	<!-- 根据条件查询记录数 -->
    <select id="countTotal" parameterType="net.oschina.durcframework.easymybatis.query.Pageable"
		resultType="long">
		SELECT count(*) FROM [${table.tableName}] t
		<include refid="common.where" />
    </select>

	<!-- 根据主键获取单条记录 -->
    <select id="get" resultMap="queryResultMap" parameterType="${context.className}">
		SELECT 	TOP 1
       		<include refid="baseColumns" />
		FROM [${table.tableName}] t
		WHERE [${context.pkName}] = #{${context.javaPkName},jdbcType=${context.mybatisPkType}}
	</select>

	<!-- 根据条件获取单条记录 -->
    <select id="getByExpression" resultMap="queryResultMap" parameterType="net.oschina.durcframework.easymybatis.query.Pageable">
		SELECT TOP 1
			<include refid="common.sqlColumns" />			
		FROM [${table.tableName}] t
		<include refid="common.where" />
		<include refid="common.orderBy" />
	</select>

	<!-- 根据属性获取单条记录 -->
	<select id="getByProperty" resultMap="queryResultMap">
		SELECT TOP 1
			<include refid="baseColumns" />
		FROM ${table.tableName} t
		WHERE t.[${jq}{column}] = #{value}
	</select>
	
	<!-- 根据属性获取多条记录 -->
	<select id="listByProperty" resultMap="queryResultMap">
		SELECT
			<include refid="baseColumns" />
		FROM [${table.tableName}] t
		WHERE t.[${jq}{column}] = #{value}
	</select>

	<!-- 保存,保存全部字段 -->
    <insert id="save" parameterType="${context.className}"
#if(${pkColumn.isIdentity})
    keyProperty="${context.javaPkName}" keyColumn="${context.pkName}" useGeneratedKeys="true"
#end
    >
	INSERT INTO [${table.tableName}]
         (
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end [${column.columnName}]
#set($i=$i +1)
            #end          
        #end
          )
	VALUES (
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end #{${column.javaFieldName},jdbcType=${column.mybatisJdbcType}}
#set($i=$i +1)
            #end          
        #end
 
        )
	</insert>
    
    <!-- 保存,保存不为NULL的字段 -->
    <insert id="saveIgnoreNull" parameterType="${context.className}"
    #if(${pkColumn.isIdentity})
        keyProperty="${context.javaPkName}" keyColumn="${context.pkName}" useGeneratedKeys="true"
    #end
        >
        INSERT INTO [${table.tableName}]
    
        <trim prefix="(" suffix=")" suffixOverrides=",">	 

            #foreach($column in $columns) 
                #if(!${column.isIdentityPk}) 
                    <if test="${column.javaFieldName} != null">
                   [${column.columnName}],
                    </if>
                #end          
            #end
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">            
            #foreach($column in $columns) 
                #if(!${column.isIdentityPk})  
                    <if test="${column.javaFieldName} != null" >
                    #{${column.javaFieldName},jdbcType=${column.mybatisJdbcType}},                    
                    </if>
                #end          
            #end
        </trim>
    </insert>
    
    <!-- 批量保存,保存全部字段 -->
    <insert id="saveBatch">
		INSERT INTO [${table.tableName}] 
		(
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end [${column.columnName}]
#set($i=$i +1)
            #end          
        #end
          )
		VALUES 
		<foreach collection="entitys" item="entity"  separator=",">
        ( #set ($i=0)
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
         	#if($i > 0),#end #{entity.${column.javaFieldName}}
         	#set($i=$i +1)
            #end          
        #end
        )
		</foreach>
	</insert>
	
	<!-- 批量保存,保存指定字段 -->
    <insert id="saveBatchWithColumns">
		INSERT INTO [${table.tableName}]
		<foreach collection="columns" item="column" open="(" close=")" separator=",">
			${jq}{column.columnName}
		</foreach>
		VALUES 
		<foreach collection="entitys" item="entity"  separator=",">
        ( 
        	<foreach collection="columns" item="column"  separator=",">
        		#{entity.${column.alias}}
        	</foreach>
        )
        </foreach>
	</insert>
	
	 <!-- 批量保存(兼容),保存全部字段 -->
    <insert id="saveMulti">
		INSERT INTO [${table.tableName}] 
		(
 #set ($i=0) 
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
        #if($i > 0),#end [${column.columnName}]
#set($i=$i +1)
            #end          
        #end
          )
		<foreach collection="entitys" item="entity"  separator="UNION ALL">
        SELECT  #set ($i=0)
        #foreach($column in $columns) 
            #if(!${column.isIdentityPk})               
         	#if($i > 0),#end #{entity.${column.javaFieldName}}
         	#set($i=$i +1)
            #end          
        #end        
		</foreach>
	</insert>
	
	<!-- 批量保存(兼容),保存指定字段 -->
    <insert id="saveMultiWithColumns">
		INSERT INTO [${table.tableName}]
		<foreach collection="columns" item="column" open="(" close=")" separator=",">
			[${jq}{column.columnName}]
		</foreach>
		<foreach collection="entitys" item="entity"  separator="UNION ALL">
        SELECT  
        	<foreach collection="columns" item="column"  separator=",">
        		#{entity.${column.alias}}
        	</foreach>
        </foreach>
	</insert>

	<!-- 更新,更新全部字段 -->
    <update id="update" parameterType="${context.className}">
    UPDATE [${table.tableName}]
     <set>		
     #foreach($column in $columns) 
         #if(!${column.isPk})               
         [${column.columnName}]=#{${column.javaFieldName},jdbcType=${column.mybatisJdbcType}},        
         #end          
     #end
     </set>	
    WHERE [${context.pkName}] = #{${context.javaPkName},jdbcType=${context.mybatisPkType}}
    </update>
    
    <!-- 更新不为NULL的字段 -->
    <update id="updateIgnoreNull" parameterType="${context.className}">
    UPDATE [${table.tableName}]
    <set>
    	#foreach($column in $columns) 
            #if(!${column.isPk})  
                <if test="${column.javaFieldName} != null" >
                [${column.columnName}]=#{${column.javaFieldName},jdbcType=${column.mybatisJdbcType}},                 
                </if>
            #end          
        #end
    </set>
    WHERE [${context.pkName}] = #{${context.javaPkName},jdbcType=${context.mybatisPkType}}
    </update>
    
    <!-- 根据指定条件更新不为NULL的字段 -->
    <update id="updateIgnoreNullByExpression">
    UPDATE [${table.tableName}]
    <set>
    	#foreach($column in $columns) 
            #if(!${column.isPk})  
                <if test="entity.${column.javaFieldName} != null" >
                [${column.columnName}]=#{entity.${column.javaFieldName},jdbcType=${column.mybatisJdbcType}},                 
                </if>
            #end          
        #end
    </set>
    <include refid="common.updateWhere" />
    </update>   
    
	
	<!-- 根据主键删除记录 -->
	<delete id="del" parameterType="${context.className}">
		DELETE FROM [${table.tableName}]
		WHERE [${context.pkName}] = #{${context.javaPkName},jdbcType=${context.mybatisPkType}}
	</delete>    
    
    <!-- 根据条件删除记录 -->
    <delete id="delByExpression" parameterType="net.oschina.durcframework.easymybatis.query.Pageable">
		DELETE FROM [${table.tableName}]
		<include refid="common.where" />
	</delete>


</mapper>